Here we will learn about setting up multiple sub-domains on a single AWS EC2 instance.

<p>
Let's say you have a domain name called <b><code>awsplayground.online</code></b> and you want to set up two sub-domains on it, <b><code>subdomain1.awsplayground.online</b></code> and <b><code>subdomain2.awsplayground.online</b></code>. You can do this by setting up two virtual hosts on your Apache server. This tutorial will show you how to do that. Let's get started.
</p>


### Step 1: Make a Directory for Each Site

```bash
sudo mkdir -p /var/www/subdomain1.awsplayground.online
sudo mkdir -p /var/www/subdomain2.awsplayground.online
sudo mkdir -p /var/www/awsplayground.online
```

### Step 2: Set Folder Permissions

```bash
sudo chmod -R 755 /var/www
```

### Step 3: Set up an Index Pages

```bash
sudo vim /var/www/subdomain1.awsplayground.online/index.html
```

Write this in the file:

```bash
<h1> This is sub domain 1</h1>
```

Repeat for 2nd sub-domain:

```bash
sudo vim /var/www/subdomain2.awsplayground.online/index.html
```

```bash
<h1> This is sub domain 2</h1>
```

Repeat for main-domain:

```bash
sudo vim /var/www/awsplayground.online/index.html
```

```bash
<h1> This is main domain</h1>
```

### Step 4: Copy the Config File for Each Site

Copy the default configuration file for each site, this will also ensure that you always have a default copy for future site creation:

```bash
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/001-subdomain1.awsplayground.online.conf
```

```bash
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/002-subdomain2.awsplayground.online.conf
```

```bash
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/999-awsplayground.online.conf
```

<code>Note - The reason behind providing prefix numbers on sub-domains and domain file is that - Apache loads things alphabetically so, if the subdomain config is after the main domain, there is a good chance that requests to the subdomain are being incorrectly routed to the primary. Generally what I do with Apache is have the primary site last in a list of config files. For more details on this, see this Stack Overflow post - </code> <a href="https://askubuntu.com/questions/1320969/apache2-subdomain-doesnt-load-content-mirrors-main-domain" target="_blank">https://askubuntu.com/questions/1320969/apache2-subdomain-doesnt-load-content-mirrors-main-domain</a>

### Step 5: Edit the Config File for main domain & subdomains

```bash
sudo vim /etc/apache2/sites-available/001-subdomain1.awsplayground.online.conf
```

### Step 6: Put this in the file:

```bash
### awsplayground.online
<VirtualHost *:80>
ServerName subdomain1.awsplayground.online

ServerAdmin admin@awsplayground.online
DocumentRoot /var/www/subdomain1.awsplayground.online/

<Directory "/var/www/subdomain1.awsplayground.online/">
    Options Indexes FollowSymLinks MultiViews
    #AllowOverride All
    Require all granted
</Directory>

ErrorLog ${APACHE_LOG_DIR}/subdomain1.awsplayground.online.error.log
CustomLog ${APACHE_LOG_DIR}/subdomain1.awsplayground.online.access.log combined
</VirtualHost>
```

<i>Make sure the DocumentRoot is the same as the directory you created in ### step 1</i>

Repeat the same steps for the rest of domain and subdomains.

### Step 7: Disable default config & Enable new config files

Disable the default file and enable the new ones you just created:
Disable the all default conf files with this command:

```bash
sudo a2dissite 000-default.conf default-ssl.conf
```

Enable both sub-domain files with this command:

```bash
sudo a2ensite 001-subdomain1.awsplayground.online.conf
sudo a2ensite 001-subdomain1.awsplayground.online-ssl.conf
sudo a2ensite 002-subdomain2.awsplayground.online.conf
sudo a2ensite 002-subdomain2.awsplayground.online-ssl.conf
sudo a2ensite 999-awsplayground.online.conf
sudo a2ensite 999-awsplayground.online-ssl.conf
```

### Step 8: Restart Apache for changes to take effect:

```bash
sudo systemctl restart apache2
```

### Step 9: Make sure to set up subdomains on your domain registrar or Route 53