
### Step 1: Connect to your EC2 instance

```sh
ssh -i /path/to/your-key.pem ubuntu@your-ec2-ip
```

### Step 2: Update and upgrade packages

```sh
sudo apt update
sudo apt upgrade
```

### Step 3: Install Apache, PHP, and Necessary Modules

```sh
sudo apt install apache2
sudo apt install php libapache2-mod-php php-mysql php-mbstring php-xml php-json
```

### Step 4: Install MySQL Server

```sh
sudo apt install mysql-server
```

### Step 5: Secure MySQL Installation

```sh
sudo mysql_secure_installation
```

Follow the prompts to set up a secure MySQL installation.

### Step 6: Create MySQL Database and User for Laravel

```sh
sudo mysql -u root -p
```

Enter the MySQL root password when prompted.

```sql
CREATE DATABASE laravel_database;
CREATE USER 'laravel_user'@'localhost' IDENTIFIED BY 'your_password';
GRANT ALL PRIVILEGES ON laravel_database.* TO 'laravel_user'@'localhost';
FLUSH PRIVILEGES;
EXIT;
```

### Step 7: Update Laravel .env File

Navigate to your Laravel project directory and update the .env file with the MySQL database configuration:

```sh
cd /var/www/html/your-laravel-repo
sudo nano .env
```

Update the following lines:

```ini
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel_database
DB_USERNAME=laravel_user
DB_PASSWORD=your_password
```

### Step 8: Install Composer and Laravel Dependencies

```sh
sudo apt install composer
cd your-laravel-repo
sudo composer install
```

### Step 9: Generate Laravel Application Key

```sh
sudo php artisan key:generate
```

### Step 10: Set Proper Permissions

```sh
sudo chown -R www-data:www-data /var/www/html/your-laravel-repo
sudo chmod -R 755 /var/www/html/your-laravel-repo/storage
```

### Step 11: Configure Apache

Create a new virtual host configuration file for your Laravel app:

```sh
sudo nano /etc/apache2/sites-available/your-laravel-repo.conf
```

Add the following configuration:

```apache
<VirtualHost *:80>
    ServerAdmin webmaster@your-laravel-repo
    DocumentRoot /var/www/html/your-laravel-repo/public

    <Directory /var/www/html/your-laravel-repo>
        AllowOverride All
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Enable the new site and restart Apache:

```sh
sudo a2ensite your-laravel-repo.conf
sudo systemctl restart apache2
```

### Step 12: Migrate Laravel Database

```sh
sudo php artisan migrate
```

### Step 13: Enable Necessary Apache Modules

```sh
sudo a2enmod rewrite
sudo systemctl restart apache2
```

Your Laravel application is now deployed on an AWS EC2 instance with MySQL. Ensure your security groups are configured to allow HTTP traffic on port 80. Consider additional security measures for a production environment, such as setting up a firewall (e.g., UFW) and configuring SSL for HTTPS.