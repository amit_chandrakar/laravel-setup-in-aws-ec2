Setup multiple domains on apache2 ubuntu server

- location: /var/domain
    - frontend.domain.com
        - htdocs
            - current
            - releases
            - shared
        - logs
        - backup
            - code (it is already in htdocs/releases folder)
            - database

    - backend.domain.com
        - htdocs
            - current
            - releases
            - shared
        - logs
        - backup
            - code (it is already in htdocs/releases folder)
            - database
