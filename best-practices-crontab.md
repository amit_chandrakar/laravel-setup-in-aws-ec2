# It is a very good practise to log the runned commands in a file. This can be done by using the following command:

Step 1: Create a bash file in /root/scripts/ directory

    touch /root/scripts/commands.sh

Step 2: Open the file in vi editor and add the following lines in it:

    date=`date +%d-%b-%Y_%T`
    file="test_command_file_$date"
    /var/domain/backend.com/htdocs/current/bin/cake test_command > /root/scripts/logs/$file

Step 3: Save the file and exit from the editor.

Step 4: Now, open the crontab file in vi editor and add the following line in it:

    0 0 * * * /bin/bash /root/scripts/commands.sh

Step 5: Now we can see all the commands runned by the cron in the file like

    /root/scripts/logs/test_command_file_01-Jan-2019_00:00:00