The repository contains documentation detailing the setup procedures for various AWS services required to establish the project. Please refer to the respective Markdown files in the repository for more information tailored to the specific service you are interested in.

### Table of Contents

<table width="100%">
    <thead>
        <tr>
            <th>Feature</th>
            <th>Description</th>
            <th>Status</th>
            <th>URL</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Setup EC2 Instance for Laravel Application
            </td>
            <td>
                Includes installation of Apache, PHP, Composer, MySQL, git, and Laravel.
            </td>
            <td>
                <img src="https://img.shields.io/badge/Published-8A2BE2green" alt="Published" />
            </td>
            <td>
                <a href="Configure-ec2-for-laravel.md">Click Here</a>
            </td>
        </tr>
        <tr>
            <td>
                Setup subdomains
            </td>
            <td>
                This document describes the steps to setup multiple subdomains for a single domain. It uses route 53 and apache's virtual host.
            </td>
            <td>
                <img src="https://img.shields.io/badge/Published-8A2BE2green" alt="Published" />
            </td>
            <td>
                <a href="Setup-subdomain.md">Click Here</a>
            </td>
        </tr>
        <tr>
            <td>
                Setting up CI/CD pipeline for Laravel application
            </td>
            <td>
                This document describes the steps to setup CI/CD pipeline for Laravel application using gitlab and AWS.
            </td>
            <td>
                <img src="https://img.shields.io/badge/TODO-red" alt="TODO" />
            </td>
            <td>
                --
            </td>
        </tr>
        <tr>
            <td>
                Setting up multiple domains for a single EC2 instance
            </td>
            <td>
                This document describes the steps to setup multiple domains for a single EC2 instance.
            </td>
            <td>
                <img src="https://img.shields.io/badge/TODO-red" alt="TODO" />
            </td>
            <td>
                --
            </td>
        </tr>
        <tr>
            <td>
                Configure free SSL
            </td>
            <td>
                This document describes the steps to configure free SSL for a domain using letsencrypt.
            </td>
            <td>
                <img src="https://img.shields.io/badge/TODO-red" alt="TODO" />
            </td>
            <td>
                --
            </td>
        </tr>
    </tbody>
</table>

