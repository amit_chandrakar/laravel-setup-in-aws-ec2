### 1. Download a file from server to local machine
    scp -i key-pair.pem user@host:<SERVER_PATH>/<FILE_NAME> <LOCAL_DIR>

#### Note - 1: If you want to download a directory, it is better to compress the directory and then download it. Follow the below steps to compress a directory.

    Step 1: Check if zip module is installed or not in your server.
        zip --version

    Step 2: If zip module is not installed, install it using the following command.
        sudo apt-get install zip

    Step 3: Compress the directory using the following command.
        zip -r <ZIP_FILE_NAME> <DIRECTORY_PATH>

    Step 4: Download the compressed file using the following command.
        scp -i key-pair.pem user@host:<SERVER_PATH>/<ZIP_FILE_NAME> <LOCAL_DIR>

### 2. Upload a file from local machine to server
    scp -i key-pair.pem <LOCAL_PATH>/<FILE_NAME> user@host:<SERVER_PATH>

Note: username must have write permission to the server path. If not, give the write permission to the user.
    chown -R <USER_NAME>:<USER_NAME> <SERVER_PATH>

Note: If you want to give write permission to all users, use the following command.
    chmod -R 777 <SERVER_PATH>

### 3. Count total number of files in a directory
    ls -l | wc -l

### 4. Show last updates on a file
    tail -f <FILE_NAME>

# TODO
- [ ] grep command
- [ ] sed command
- [ ] find command
- [ ] curl command
- [ ] wget command
- [ ] scp command
- [ ] crontab command