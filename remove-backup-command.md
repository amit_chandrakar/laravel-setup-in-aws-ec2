# Keep only latest 5 backups and delete the rest

Step 1: Create a script file
    touch /root/scripts/remove_old_releases.sh

Step 2: Add the following content to the file
    #!/bin/bash
    # Remove old releases
    date
    ls -td /var/domain/frontend.com/htdocs/releases/* | tail -n +6 | xargs -I {} echo "Removing directory {}"
    ls -td /var/domain/frontend.com/htdocs/releases/* | tail -n +6 | xargs -I {} rm -rf {}
    date
    echo "All releases except latest 5 were removed!"

Step 3: Make the script executable
    chmod +x /root/scripts/remove_old_releases.sh

Step 4: Add the following line to the crontab and run it every day at 00:00
    0 0 * * * /root/scripts/remove_old_releases.sh >> /var/log/remove_old_releases.log

Step 5: Check the log file
    cat /var/log/remove_old_releases.log